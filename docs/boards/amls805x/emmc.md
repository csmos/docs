title: Flashing eMMC
hero: AML-S805X-AC (La Frite)

## Flashing eMMC

### Requirements [^1]

[^1]: https://forum.loverpi.com/discussion/789/howto-flash-image-to-emmc-from-a-computer

*  Install latest La Frite [firmware](../firmware).

### Connection

1.  Connect a USB Type A to USB Type A cable to your computer.

!!! info
    USB port to use in La Frite is the closest to the IR sensor and to the pin
    header.

2.  When booting, press `escape` to go into the boot menu.

3.  Select `eMMC USB Drive Mode`.

!!! success
    On your computer, the eMMC module should now be listed as a block storage
    device.

Logs:

=== "Boot menu"

    ```shell hl_lines="8"

      *** U-Boot Boot Menu ***

         Boot
         Boot USB
         Boot PXE
         Boot DHCP
         EtherealOS
         eMMC USB Drive Mode
         fastboot USB Mode
         Reboot
         U-Boot console


      Press UP/DOWN to move, ENTER to select
    ```

=== "eMMC USB Drive Mode (host dmesg output)"

    ```shell
    [ 4596.518464] usb 1-3.1.3: new high-speed USB device number 13 using xhci_hcd
    [ 4596.518975] usb 1-3.1.3: Device not responding to setup address.
    [ 4601.862350] xhci_hcd 0000:00:14.0: Timeout while waiting for setup device command
    [ 4602.070255] usb 1-3.1.3: device not accepting address 13, error -62
    [ 4602.150301] usb 1-3.1.3: new high-speed USB device number 14 using xhci_hcd
    [ 4602.171132] usb 1-3.1.3: New USB device found, idVendor=1b8e, idProduct=fada, bcdDevice=99.99
    [ 4602.171137] usb 1-3.1.3: New USB device strings: Mfr=1, Product=2, SerialNumber=0
    [ 4602.171141] usb 1-3.1.3: Product: USB download gadget
    [ 4602.171143] usb 1-3.1.3: Manufacturer: Libre Computer
    [ 4602.191318] usb-storage 1-3.1.3:1.0: USB Mass Storage device detected
    [ 4602.191398] scsi host6: usb-storage 1-3.1.3:1.0
    [ 4602.191474] usbcore: registered new interface driver usb-storage
    [ 4602.193266] usbcore: registered new interface driver uas
    [ 4603.207018] scsi 6:0:0:0: Direct-Access     Linux    UMS disk 0       ffff PQ: 0 ANSI: 2
    [ 4603.207691] sd 6:0:0:0: Attached scsi generic sg4 type 0
    [ 4603.207947] sd 6:0:0:0: [sdd] 15269888 512-byte logical blocks: (7.82 GB/7.28 GiB)
    [ 4603.208083] sd 6:0:0:0: [sdd] Write Protect is off
    [ 4603.208086] sd 6:0:0:0: [sdd] Mode Sense: 0f 00 00 00
    [ 4603.208220] sd 6:0:0:0: [sdd] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
    [ 4603.254568]  sdd: sdd1 sdd2 sdd3
    [ 4603.257066] sd 6:0:0:0: [sdd] Attached SCSI removable disk
    [ 4603.890176] raid6: avx2x4   gen() 25717 MB/s
    [ 4603.958192] raid6: avx2x4   xor() 23047 MB/s
    [ 4604.026173] raid6: avx2x2   gen() 33244 MB/s
    [ 4604.094172] raid6: avx2x2   xor() 20945 MB/s
    [ 4604.162171] raid6: avx2x1   gen() 26565 MB/s
    [ 4604.230172] raid6: avx2x1   xor() 17171 MB/s
    [ 4604.298170] raid6: sse2x4   gen() 16131 MB/s
    [ 4604.366168] raid6: sse2x4   xor()  9875 MB/s
    [ 4604.434162] raid6: sse2x2   gen() 13565 MB/s
    [ 4604.502176] raid6: sse2x2   xor()  9267 MB/s
    [ 4604.570163] raid6: sse2x1   gen() 12410 MB/s
    [ 4604.638161] raid6: sse2x1   xor()  6949 MB/s
    [ 4604.638162] raid6: using algorithm avx2x2 gen() 33244 MB/s
    [ 4604.638162] raid6: .... xor() 20945 MB/s, rmw enabled
    [ 4604.638163] raid6: using avx2x2 recovery algorithm
    [ 4604.644765] xor: automatically using best checksumming function   avx
    [ 4604.693549] Btrfs loaded, crc32c=crc32c-intel
    [ 4604.695235] BTRFS: device label SYSTEM devid 1 transid 665 /dev/sdd2 scanned by systemd-udevd (8651)
    [ 4604.849219] BTRFS info (device sdd2): disk space caching is enabled
    [ 4604.849264] BTRFS info (device sdd2): has skinny extents
    [ 4604.896235] BTRFS info (device sdd2): checking UUID tree
    ```

=== "eMMC drive"

    ```shell
    sudo fdisk -l /dev/sdd

    Disk /dev/sdd: 7.29 GiB, 7818182656 bytes, 15269888 sectors
    Disk model: UMS disk 0
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: gpt
    Disk identifier: A38BEDA8-7A62-4EE1-B853-2EBB2C83C622

    Device        Start      End  Sectors  Size Type
    /dev/sdd1      2048   524287   522240  255M EFI System
    /dev/sdd2    524288 13631487 13107200  6.3G Linux root (ARM-64)
    /dev/sdd3  13631488 15269854  1638367  800M Linux swa
    ```

=== "UART"

    ```shell
    mmc@74000: 0 (eMMC)
    switch to partitions #0, OK
    mmc0(part 0) is current device
    Press Control+C to end USB Drive mode.
    UMS: LUN 0, dev 0, hwpart 0, sector 0x0, count 0xe90000
    |crq->brequest:0x0
    ```


###  Start flashing

```shell
sudo dd if=/home/pd/Downloads/libre-computer-aml-s805x-ac-debian-buster-headless-mali-4.19.64+-2019-08-05.img of=/dev/sdd
```
