title: U-Boot script
description: How to flash U-Boot script
hero: AML-S805X-AC (La Frite)

## Requirements

For any of the following procedures, you need to clone `pyamlboot`[^1] repo we've used for [flashing La Frite
firmware](../firmware-latest).

[^1]: [pyamlboot](https://github.com/libre-computer-project/pyamlboot.git): Amlogic SoC USB Boot utility

```shell
git clone https://github.com/libre-computer-project/pyamlboot.git --single-branch --branch gxl
```

### Connection

Follow the same [steps](../firmware-latest#connection) we've done for the firmware flashing.

### Flashing U-Boot

If you have generated new U-Boot image you can always flashit following the next
steps:

```
cd pyamlboot/
./flash-fw firmware-flash --fw-file u-boot-libretech-ac.bin --local
```

!!! note
	`flash-fw` is a simple tool used for downloading latest firmware
	released and it can be used to flash your custom U-Boot binary. But it
	executes the next following command:

	```bash
	sudo ./boot.py --script flash-firmware.scr --fdt /tmp/tmp137wncnv --ramfs u-boot-libretech-ac.bin aml-s805x-ac
	```

## Flashing `aml-s805x-ac` U-Boot script

### Flashing U-Boot script

Prepare a script file like `cosmos.cmd`:

```bash
echo ##### USB Boot script !! #####
setenv distro "cosmos"
kernel_loadaddr=0x11000000
dtb_mem_addr=0x1000000
setenv bootmenu_8 "Boot eMMC (booti)"="run kernel_mmc; run dtb_mmc; run bootargs_mmc; run boot_image_mmc"
setenv kernel_mmc "ext2load mmc 0:1 ${kernel_loadaddr} /boot/Image"
setenv dtb_mmc "ext2load mmc 0:1 ${dtb_mem_addr} /boot/meson-gxl-s805x-libretech-ac.dtb"
setenv bootargs_mmc "setenv bootargs "console=ttyAML0,115200n8 console=tty0 no_console_suspend consoleblank=0 root=/dev/mmcblk0p1 rootwait""
setenv boot_image_mmc "booti ${kernel_loadaddr} - ${dtb_mem_addr}"
saveenv
sleep 1
```

Generate the U-Boot script file based on the previous file we have just
generated:

```bash
mkimage -C none -A arm -T script -d cosmos.cmd cosmos.scr
```

Log:

```bash
Image Name:
Created:      Sun Jun 28 13:16:08 2020
Image Type:   ARM Linux Script (uncompressed)
Data Size:    604 Bytes = 0.59 KiB = 0.00 MiB
Load Address: 00000000
Entry Point:  00000000
Contents:
   Image 0: 596 Bytes = 0.58 KiB = 0.00 MiB
```

Follow the [connection](firmware#connection) steps before flashing the U-Boot
script.

### Start flashing U-Boot script

```bash
$ sudo ./boot.py --script cosmos.scr aml-s805x-ac
```

!!! note
	If needed, restore the default environment.

	```bash
	env default -a
	```

On the board, select `U-Boot console` and load the file:

```
=> source 08000000
```

!!! warning
	Don't let the board autoboot. Cancel the U-Boot timeout and go to the
	`U-Boot console`.

Then, power cycle the board and you check the new variable is present:

```
printenv distro
```

Logs:

```
=> printenv distro
## Error: "distro" not defined

=>source 08000000
## Executing script at 08000000

Saving Environment to SPI Flash... SF: Detected gd25lq128 with page size 256 Bytes, erase size 64 KiB, total 16 MiB
Erasing SPI flash...Writing to SPI flash...done
OK
=> printenv distro
distro=cosmos
=> printenv bootmenu_8
bootmenu_8=Boot eMMC (booti)=run kernel_mmc; run dtb_mmc; run bootargs_mmc; run boot_image_mmc
```
