title: Linux kernel
hero: AML-S805X-AC (La Frite)

## Requirements [^1]

[^1]: https://forum.loverpi.com/discussion/789/howto-flash-image-to-emmc-from-a-computer

*  Install latest La Frite [firmware][1].

  [1]: ../firmware


## Linux kernel :fontawesome-brands-linux:


```shell
git clone https://github.com/libre-computer-project/libretech-linux.git
```


```shell
make ARCH=arm64 defconfig
```

```shell
make CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 -j `nproc` \
deb-pkg amlogic/meson-gxl-s805x-libretech-ac.dtb
```





