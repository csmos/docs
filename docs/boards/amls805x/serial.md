title: Serial
description: How to use UART
hero: AML-S805X-AC (La Frite)

## Serial information[^1]

[^1]: https://forum.loverpi.com/discussion/792/developer-guide-for-aml-s805x-ac-la-frite


*  Configuration: `115200 8N1`

Pin | Function
----|---------
3 | TX
5 | RX
6 | GND

## Debug console

*  Check where the UART cable is connected:

```shell
dmesg -w
```

Logs:

```shell

[ 1142.903109] usb 1-11: new full-speed USB device number 8 using xhci_hcd
[ 1143.051833] usb 1-11: New USB device found, idVendor=067b, idProduct=2303, bcdDevice= 3.00
[ 1143.051838] usb 1-11: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[ 1143.051842] usb 1-11: Product: USB-Serial Controller
[ 1143.051844] usb 1-11: Manufacturer: Prolific Technology Inc.
[ 1143.053596] pl2303 1-11:1.0: pl2303 converter detected
[ 1143.054572] usb 1-11: pl2303 converter now attached to ttyUSB0
```

*  Open the console with `picocom` or `minicom` or similar:

```shell
picocom -b 115200 /dev/ttyUSB0
```
