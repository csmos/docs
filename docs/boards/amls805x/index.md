title: Summary
hero: AML-S805X-AC (La Frite)

## AML-S805X-AC

![lafrite-yocto](../../assets/lafrite/lafrite.png)

## Links

Link | Description
-----|------------
AML-S805X-AC (La Frite) | https://libre.computer/products/boards/aml-s805x-ac/
Forum | https://forum.loverpi.com/categories/libre-computer-aml-s805x-ac
Wiki (SoC) | http://wiki.loverpi.com/soc:amlogic-s805x | Wiki (SoC)
Wiki (sbc) | http://wiki.loverpi.com/sbc:libre-computer-aml-s805x-ac | Wiki (sbc)

![lafrite-u-boot-button](../../assets/lafrite/lafrite-u-boot-button.png)
