title: Firmware
hero: AML-S805X-AC (La Frite)

## Flashing `aml-s805x-ac` firmware

### Requirements [^1]

[^1]: https://forum.loverpi.com/discussion/740/howto-update-flash-re-flash-la-frite-firmware-from-another-linux-computer

*  Install required packages on your computer:

```shell
sudo apt-get install git wget python3 python3-usb
```


*  Clone `pyamlboot`[^2]  repo.

[^2]: [pyamlboot](https://github.com/libre-computer-project/pyamlboot.git): Amlogic SoC USB Boot utility

```shell
git clone https://github.com/libre-computer-project/pyamlboot.git --single-branch --branch gxl
```

*  Install dependencies.

```shell
pip3 install -r pyamlboot/flash-fw-requirements.txt
```

### Connection

!!! note
    You can connect the serial port to see the flashing progress.

    ```shell
    picocom -b 115200 /dev/ttyUSB0
    ```
1.  Connect a USB Type A to USB Type A cable to your computer.

![lafrite-u-boot-button](../../assets/lafrite/lafrite-u-boot-button.png)

2.  Hold down 'Uboot' button on La Frite and connect the USB type A from your
    computer to La Frite.

!!! info
    USB port to use in La Frite is the closest to the IR sensor and to the pin
    header.

Logs:

=== "dmesg"

    ```shell
    [ 2449.189746] usb 1-3.1.3: new high-speed USB device number 10 using xhci_hcd
    [ 2449.210745] usb 1-3.1.3: New USB device found, idVendor=1b8e, idProduct=c003, bcdDevice= 0.20
    [ 2449.210746] usb 1-3.1.3: New USB device strings: Mfr=1, Product=2, SerialNumber=0
    [ 2449.210747] usb 1-3.1.3: Product: GX-CHIP
    [ 2449.210748] usb 1-3.1.3: Manufacturer: Amlogic
    ```

=== "UART"

    ```shell
    GXL:BL1:9ac50e:bb16dc;FEAT:ADFC318C:0;POC:0;RCY:0;USB:0;
    ```

###  Check available firmware

```shell
./pyamlboot/flash-fw firmware --board aml-s805x-ac
```

Output:

```shell
Available firmware for 'aml-s805x-ac' board:
  file: latest.bin
  file: u-boot-2019-04-r1.bin
  file: u-boot-2019-04-r2.bin
  file: u-boot-2019-04-r3.bin
  file: u-boot-2019-04-r4.bin
  file: u-boot-2019-04-r5.bin
  file: u-boot-2020-04-r1.bin
  file: u-boot-2020-04-r2.bin
  file: u-boot-2020-04-r3.bin
```

###  Start flashing

In this case, we will use `latest.bin` which points to the latest firmware
available. At the moment of writing this is `u-boot-2020-04-r3.bin`.

!!! tip
    You can also check the flashing status on the HDMI output. If you do,
    you will be sure the flashing procedure finishes correctly.

```shell
cd pyamlboot
./flash-fw firmware-flash --board aml-s805x-ac --fw-file latest.bin
```

Logs:

=== "Output"

    ```shell
    $ ./flash-fw firmware-flash --board aml-s805x-ac --fw-file latest.bin
    Firmware 'latest.bin' downloading for 'aml-s805x-ac' board
    Downloading:
    http://share.loverpi.com/board/libre-computer-project/libre-computer-board/aml-s805x-ac/firmware/latest.bin
    100% [........................................................................] 16611967 / 16611967sh: 1: Syntax error: end of file unexpected
    flashing cmd: sudo ./boot.py --script flash-firmware.scr --fdt /tmp/tmp40d88480 --ramfs latest.bin aml-s805x-ac

    Using GX Family boot parameters
    ROM: 2.4 Stage: 0.0
    Writing /home/daniel/wd/amls805x/pyamlboot/files/aml-s805x-ac/u-boot.bin.usb.bl2 at 0xd9000000...
    [DONE]
    Writing /home/daniel/wd/amls805x/pyamlboot/files/usbbl2runpara_ddrinit.bin at 0xd900c000...
    [DONE]
    Running at 0xd9000000...
    [DONE]
    Waiting...
    [DONE]
    ROM: 2.2 Stage: 0.8
    Running at 0xd900c000...
    [DONE]
    Waiting...
    [DONE]
    Writing /home/daniel/wd/amls805x/pyamlboot/files/aml-s805x-ac/u-boot.bin.usb.bl2 at 0xd9000000...
    [DONE]
    Writing /home/daniel/wd/amls805x/pyamlboot/files/usbbl2runpara_runfipimg.bin at 0xd900c000...
    [DONE]
    Writing /home/daniel/wd/amls805x/pyamlboot/files/aml-s805x-ac/u-boot.bin.usb.tpl at 0x200c000...
    [DONE]
    Writing /tmp/tmp40d88480 at 0x8008000...
    [DONE]
    Writing flash-firmware.scr at 0x8000000...
    [DONE]
    Writing latest.bin at 0x13000000...
    [DONE]
    Running at 0xd900c000...
    [DONE]
    ```

=== "UART"

    ```shell
    GXL:BL1:9ac50e:bb16dc;FEAT:ADFC318C:0;POC:0;RCY:0;USB:0;0.0;CHK:0;
    TE: 15714845

    BL2 Built : 15:21:18, Aug 28 2019. gxl g1bf2b53 - luan.yuan@droid15-sz

    set vcck to 1120 mv
    set vddee to 1000 mv
    Board ID = 11
    CPU clk: 1200MHz
    BL2 USB
    DDR enable rdbi
    DDR use ext vref
    DQS-corr enabled
    DDR scramble enabled
    DDR4 chl: Rank0 16bit @ 1200MHz

    bist_test rank: 0 13 00 26 2b 17 3f 13 00 27 2c 19 3f 1f 1f 1f 1f 1f 1f 1f 1f 1f 1f 1f 1f 661
    Rank0: 1024MB(auto)-2T-18
    Set ddr ssc: ppm1000-
    Load fip header from USB, src: 0x0000c000, des: 0x01400000, size: 0x00004000, part: 0
    New fip structure!
    Load bl30 from USB, src: 0x00010000, des: 0x013c0000, size: 0x0000d600, part: 0
    Load bl31 from USB, src: 0x00020000, des: 0x05100000, size: 0x0002b400, part: 0
    Load bl33 from USB, src: 0x0004c000, des: 0x01000000, size: 0x00092000, part: 0
    NOTICE:  BL3-1: v1.0(release):53f813e
    NOTICE:  BL3-1: Built : 15:51:23, May 22 2019
    [BL31]: GXL CPU setup!
    NOTICE:  BL3-1: GXL normal boot!
    NOTICE:  BL3-1: BL33 decompress pass
    mpu_config_enable:system pre init ok
    dmc sec lock
    [Image: gxl_v1.1.3390-6ac5299 2019-09-26 14:09:24 luan.yuan@droid15-sz]
    OPS=0x34
    21 0d 34 00 e9 b5 40 53 5b 00 ad 6c 12 e4 21 02
    [29.796041 Inits done]
    secure task start!
    high task start!
    low task start!
    ERROR:   Error initializing runtime service opteed_fast


    U-Boot 2020.04+ (Jun 11 2020 - 02:06:49 +0000) Libre Computer AML-S805X-AC

    DRAM:  1 GiB
    MMC:   mmc@74000: 0
    Loading Environment from SPI Flash... SF: Detected gd25lq128 with page size 256 Bytes, erase size 64 KiB, total 16 MiB
    OK
    Error (-1): cannot determine file size
    In:    serial
    Out:   serial
    Err:   serial
    [BL31]: tee size: 0
    Net:   eth0: ethernet@c9410000
    starting USB...
    Bus dwc3@c9000000: Register 2000140 NbrPorts 2
    Starting the controller
    USB XHCI 1.00
    scanning bus dwc3@c9000000 for devices... 1 USB Device(s) found
           scanning usb for storage devices... 0 Storage Device(s) found
    Hit any key to stop autoboot:  0


      *** U-Boot Boot Menu ***

         Boot
         Boot USB
         Boot eMMC
         Boot PXE
         Boot DHCP
         eMMC USB Drive Mode
         Detect USB Devices
         Reboot
         U-Boot console


      Press UP/DOWN to move, ENTER to select
    ```

### Troubleshooting

#### Error: `usb.core.USBError: [Errno 110] Operation timed out`

If you get the next following error when you are trying to flash the board
firmware:

```shell
Traceback (most recent call last):
  File "./boot.py", line 126, in <module>
    usb.load_uboot()
  File "./boot.py", line 88, in load_uboot
    self.write_file(os.path.join(self.bpath, self.TPL_FILE), self.UBOOT_LOAD, large = 64, fill = True)
  File "./boot.py", line 62, in write_file
    self.dev.writeLargeMemory(addr, b, large, fill)
  File "/home/daniel/wd/amls805x/pyamlboot/pyamlboot/pyamlboot.py", line 232, in writeLargeMemory
    self._writeLargeMemory(address+offset, data[offset:offset+writeLength], \
  File "/home/daniel/wd/amls805x/pyamlboot/pyamlboot/pyamlboot.py", line 213, in _writeLargeMemory
    ep.write(data[offset:offset+blockLength], 1000)
  File "/usr/lib/python3/dist-packages/usb/core.py", line 387, in write
    return self.device.write(self, data, timeout)
  File "/usr/lib/python3/dist-packages/usb/core.py", line 943, in write
    return fn(
  File "/usr/lib/python3/dist-packages/usb/backend/libusb1.py", line 819, in bulk_write
    return self.__write(self.lib.libusb_bulk_transfer,
  File "/usr/lib/python3/dist-packages/usb/backend/libusb1.py", line 920, in __write
    _check(retval)
  File "/usr/lib/python3/dist-packages/usb/backend/libusb1.py", line 595, in _check
    raise USBError(_strerror(ret), ret, _libusb_errno[ret])
usb.core.USBError: [Errno 110] Operation timed out
```

Just be sure you are using an USB 2.0 and not an USB 3.0 port in your machine.
