# Introduction

csmos documentation.

# Build locally

Clone the documentation repo and run the following:

```
docker pull \
registry.gitlab.com/csmos/dockerhub/mkdocs-material:latest
```

```
docker run --rm -it \
-p 8000:8000 \
-v ${PWD}:/docs \
registry.gitlab.com/csmos/dockerhub/mkdocs-material:latest
```

Documentation will be served at [http://0.0.0.0:8000](http://0.0.0.0:8000).
